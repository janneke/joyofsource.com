title: GNU Mes 0.24.1 released
date: 2022-10-16 11:00:00 +0200
tags: mes
summary: mes/ANNOUNCE-0.24.1
---
We are happy to announce the release of GNU Mes 0.24.1, representing 23
commits over five months by four people.

Mes now supports the stage0-posix and M2-Planet Full Source bootstrap on
Aarch64 for ARM.  Because stage0-posix does not support ARM yet, the Mes
ARM bootstrap is currently prototyped on Aarch64.

We are excited that the NlNet Foundation[4] is again sponsoring this
work!

What's next?

Work to prototype this so-called "Full Source Bootstrap" for ARM on
Aarch64 is happening on the wip-aarch64-bootstrap[3] branch.  Also, full
Guile compatible module support, and RISC-V support.

Enjoy!

# About

[GNU Mes](https://www.gnu.org/software/mes) is a Scheme interpreter
and C compiler for bootstrapping the GNU System.  Since version 0.22
it has again helped to halve the size of opaque, uninspectable binary
seeds that are currently being used in the [Reduced Binary Seed
bootstrap](https://guix.gnu.org/blog/2019/guix-reduces-bootstrap-seed-by-50/)
of [GNU Guix](https://www.gnu.org/software/guix).  The final goal is
to help create a full source bootstrap as part of the [bootstrappable
builds](http://bootstrappable.org) effort for UNIX-like operating
systems.

The Scheme interpreter is written in ~5,000 LOC of simple C, and the C
compiler written in Scheme and these are mutual self-hosting.  Mes can
now be bootstrapped from
[M2-Planet](https://github.com/oriansj/m2-planet) and
[Mescc-Tools](https://savannah.nongnu.org/projects/mescc-tools).

Mes has a Garbage Collector, a library of loadable Scheme modules--
notably Dominique Boucher's
[LALR](https://github.com/schemeway/lalr-scm), Pre-R6RS [portable
syntax-case](https://www.cs.indiana.edu/chezscheme/syntax-case/old-psyntax.html)
with R7RS ellipsis, Matt Wette's [Nyacc](https://www.nongnu.org/nyacc)
--and test suite, just enough to support a REPL and a C99 compiler:
mescc.

Mes + MesCC + Mes C Library can build a [bootstrappable
TinyCC](https://gitlab.com/janneke/tinycc) that is self-hosting.
Using this tcc and the Mes C library we now have a Reduced Binary Seed
bootstrap for the gnutools triplet: glibc-2.2.5, binutils-2.20.1,
gcc-2.95.3.  This is enough to bootstrap Guix for i686-linux,
x86_64-linux, armhf-linux and aarch64-linux.

Mes is inspired by The Maxwell Equations of Software:
[LISP-1.5](http://www.softwarepreservation.org/projects/LISP/book/LISP%201.5%20Programmers%20Manual.pdf)
-- John McCarthy page 13, GNU Guix's source/binary packaging
transparency and Jeremiah Orians's
[stage0](https://github.com/oriansj/stage0) ~500 byte self-hosting hex
assembler.

# Download

    git clone git://git.savannah.gnu.org/mes.git

  Here are the compressed sources and a GPG detached signature[*]:  
    [mes-0.24.1.tar.gz](https://ftp.gnu.org/gnu/mes/mes-0.24.1.tar.gz)  
    [mes-0.24.1.tar.gz.sig](https://ftp.gnu.org/gnu/mes/mes-0.24.1.tar.gz.sig)

  Use a mirror for higher download bandwidth:  
    [mes-0.24.1.tar.gz](https://ftpmirror.gnu.org/mes/mes-0.24.1.tar.gz)  
    [mes-0.24.1.tar.gz.sig](https://ftpmirror.gnu.org/mes/mes-0.24.1.tar.gz.sig)

  Here are the SHA1 and SHA256 checksums:

    bceaaaf1cafaa31ccb1ee1247ce2dd651a2f67be  mes-0.24.1.tar.gz  
    35120ceb0676632e58973355b9f86dff9cc717ed65ed2a17ff5272c59f2a0535  mes-0.24.1.tar.gz

  [*] Use a .sig file to verify that the corresponding file (without the
  .sig suffix) is intact.  First, be sure to download both the .sig file
  and the corresponding tarball.  Then, run a command like this:

    gpg --verify mes-0.24.tar.gz.sig

  If that command fails because you don't have the required public key,
  or that public key has expired, try the following commands to update
  or refresh it, and then rerun the 'gpg --verify' command.

    gpg --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

# Get informed, get involved

  See [bootstrappable.org](https://bootstrappable.org)  
  Join #bootstrappable on irc.libera.chat

## NEWS

### Changes in 0.24.1 since 0.24
  * Build
    - Support M2-Planet bootstrap for ARM.
  * MesCC
    - The Mes C Library now supports bootstrapping ARM.
  * Noteworthy bug fixes
    - The definition of S_ISUID was fixed.
    - Unsigned modulo has been fixed for ARM.
    - A bug with abtol has been fixed.
    - Workarounds for building with gcc-12.2.0 have been added.

* Links
  0) [stage0-posix](https://github.com/oriansj/stage0-posix)
  1) [bootstrap-seeds](https://github.com/oriansj/bootstrap-seeds)
  2) [FOSDEM21 GNU Mes](https://archive.fosdem.org/2021/schedule/event/gnumes)
  3) [Guix wip-full-source-bootstrap branch](https://git.savannah.gnu.org/cgit/guix.git/log/?h=wip-full-source-bootstrap)
  4) [NLnet Full Source Bootstrap](https://nlnet.nl/project/GNUMes-ARM_RISC-V)
  5) [GNU Mes](https://www.gnu.org/software/mes)
  6) [Reduced Binary Seed Bootstrap](https://guix.gnu.org/blog/2020/guix-further-reduces-bootstrap-seed-to-25)
  7) [GNU Guix](https://www.gnu.org/software/guix)
  8) [Bootstrappable builds](https://bootstrappable.org)
  9) [M2-Planet](https://github.com/oriansj/m2-planet)
  10) [MesCC-Tools](https://savannah.nongnu.org/projects/mescc-tools)
  11) [LALR](https://github.com/schemeway/lalr-scm)
  12) [portable syntax-case](https://www.cs.indiana.edu/chezscheme/syntax-case/old-psyntax.html)
  13) [NYACC](https://www.nongnu.org/nyacc)
  14) [TinyCC](https://gitlab.com/janneke/tinycc)
  15) [LISP-1.5](http://www.softwarepreservation.org/projects/LISP/book/LISP%201.5%20Programmers%20Manual.pdf)
  16) [Stage0](https://savannah.nongnu.org/projects/stage0)
