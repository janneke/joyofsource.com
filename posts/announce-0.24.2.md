title: GNU Mes 0.24.2 released
date: 2023-02-15 9:00:00 +0100
tags: mes
summary: mes/ANNOUNCE-0.24.2
---
We are happy to announce the release of GNU Mes 0.24.2, representing 25
commits over nine months by four people.

This release should fix the long standing `stat64' bug
[#41264](https://debbugs.gnu.org/41264).

We are very grateful that the [NLnet Foundation](https://nlnet.nl) is
sponsoring this work!

# What's next?

Work to prototype this so-called "Full Source Bootstrap" for ARM on
Aarch64 is happening on the [Guix wip-aarch64-bootstrap
branch](https://git.savannah.gnu.org/cgit/guix.git/log/?h=wip-aarch64-bootstrap)
branch.  Also, full Guile compatible module support, and RISC-V
support.

Enjoy!

# About

[GNU Mes](https://www.gnu.org/software/mes) is a Scheme interpreter
and C compiler for bootstrapping the GNU System.  Since version 0.22
it has again helped to halve the size of opaque, uninspectable binary
seeds that are currently being used in the [Reduced Binary Seed
bootstrap](https://guix.gnu.org/blog/2019/guix-reduces-bootstrap-seed-by-50/)
of [GNU Guix](https://www.gnu.org/software/guix).  The final goal is
to help create a full source bootstrap as part of the [bootstrappable
builds](http://bootstrappable.org) effort for UNIX-like operating
systems.

The Scheme interpreter is written in ~5,000 LOC of simple C, and the C
compiler written in Scheme and these are mutual self-hosting.  Mes can
now be bootstrapped from
[M2-Planet](https://github.com/oriansj/m2-planet) and
[Mescc-Tools](https://savannah.nongnu.org/projects/mescc-tools).

Mes has a Garbage Collector, a library of loadable Scheme modules--
notably Dominique Boucher's
[LALR](https://github.com/schemeway/lalr-scm), Pre-R6RS [portable
syntax-case](https://www.cs.indiana.edu/chezscheme/syntax-case/old-psyntax.html)
with R7RS ellipsis, Matt Wette's [Nyacc](https://www.nongnu.org/nyacc)
--and test suite, just enough to support a REPL and a C99 compiler:
mescc.

Mes + MesCC + Mes C Library can build a [bootstrappable
TinyCC](https://gitlab.com/janneke/tinycc) that is self-hosting.
Using this tcc and the Mes C library we now have a Reduced Binary Seed
bootstrap for the gnutools triplet: glibc-2.2.5, binutils-2.20.1,
gcc-2.95.3.  This is enough to bootstrap Guix for i686-linux,
x86_64-linux, armhf-linux and aarch64-linux.

Mes is inspired by The Maxwell Equations of Software:
[LISP-1.5](http://www.softwarepreservation.org/projects/LISP/book/LISP%201.5%20Programmers%20Manual.pdf)
-- John McCarthy page 13, GNU Guix's source/binary packaging
transparency and Jeremiah Orians's
[stage0](https://github.com/oriansj/stage0) ~500 byte self-hosting hex
assembler.

# Download

    git clone git://git.savannah.gnu.org/mes.git

  Here are the compressed sources and a GPG detached signature[*]:  
    [mes-0.24.2.tar.gz](https://ftp.gnu.org/gnu/mes/mes-0.24.2.tar.gz)  
    [mes-0.24.2.tar.gz.sig](https://ftp.gnu.org/gnu/mes/mes-0.24.2.tar.gz.sig)

  Use a mirror for higher download bandwidth:  
    [mes-0.24.2.tar.gz](https://ftpmirror.gnu.org/mes/mes-0.24.2.tar.gz)  
    [mes-0.24.2.tar.gz.sig](https://ftpmirror.gnu.org/mes/mes-0.24.2.tar.gz.sig)

  Here are the SHA1 and SHA256 checksums:

    30b0ce4cd37c87dca37b85a6c19646001881be46  mes-0.24.2.tar.gz  
    7ddae0854e46ebfa18c13ab37e64839a7b86ea88aeed366a8d017efd11dae86e  mes-0.24.2.tar.gz

  [*] Use a .sig file to verify that the corresponding file (without the
  .sig suffix) is intact.  First, be sure to download both the .sig file
  and the corresponding tarball.  Then, run a command like this:

    gpg --verify mes-0.24.tar.gz.sig

  If that command fails because you don't have the required public key,
  or that public key has expired, try the following commands to update
  or refresh it, and then rerun the 'gpg --verify' command.

    gpg --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

# Get informed, get involved

  See [bootstrappable.org](https://bootstrappable.org)  
  Join #bootstrappable on irc.libera.chat

## NEWS

### Changes in 0.24.2 since 0.24.1
  * Build
    - A number of compile warnings have been fixed.
    - The `simple.make` and `simple.sh` builds have been resurrected.
  * MesCC
    - Some assembly defines were added for building TinyCC for x86_64.
    - `__assert_fail` has been updated to use the standard signature.
  * Noteworthy bug fixes
    - A bootstrap build without M2-Planet is now supported again.
    - `gettimeofday` no longer segfaults in the M2-Planet build.
    - `stat64` and friends are now used on 32bit platforms.
  This fixes [#41264](https://debbugs.gnu.org/41264), and should also fix
    [#49985](https://debbugs.gnu.org/49985),
    [#53415](https://debbugs.gnu.org/53415), and
    [#53416](https://debbugs.gnu.org/53416).
    - The Mes C Library now supports uppercase hex conversions.

* Links
  0) [#41264](https://debbugs.gnu.org/41264)
  1) [NLnet Full Source Bootstrap](https://nlnet.nl/project/GNUMes-ARM_RISC-V)
  2) [Guix wip-aarch64-bootstrap branch](https://git.savannah.gnu.org/cgit/guix.git/log/?h=wip-aarch64-bootstrap)
  3) [GNU Mes](https://www.gnu.org/software/mes)
  4) [Reduced Binary Seed Bootstrap](https://guix.gnu.org/blog/2020/guix-further-reduces-bootstrap-seed-to-25)
  5) [GNU Guix](https://www.gnu.org/software/guix)
  6) [Bootstrappable builds](https://bootstrappable.org)
  7) [M2-Planet](https://github.com/oriansj/m2-planet)
  8) [MesCC-Tools](https://savannah.nongnu.org/projects/mescc-tools)
  9) [LALR](https://github.com/schemeway/lalr-scm)
  10) [portable syntax-case](https://www.cs.indiana.edu/chezscheme/syntax-case/old-psyntax.html)
  11) [NYACC](https://www.nongnu.org/nyacc)
  12) [Bootstrappable TinyCC](https://gitlab.com/janneke/tinycc)
  13) [LISP-1.5](http://www.softwarepreservation.org/projects/LISP/book/LISP%201.5%20Programmers%20Manual.pdf)
  14) [stage0-posix](https://github.com/oriansj/stage0-posix)
