title: GNU Mes 0.19 released
date: 2018-12-16 21:02:05 +0100
tags: mes
summary: mes/ANNOUNCE-0.19
---
We are pleased to announce the release of GNU Mes 0.19, representing
100 commits over 10 weeks.

Mes has now brought the Reduced Binary Seed bootstrap to Guix (bootstrap
a GNU/Linux system without binary GNU toolchain or equivalent) and work
is ongoing to audit and verify this bootstrap path in NixOS.

This release introduces strings as byte-array, hash-tables and native
structs.  While that does increase the footprint somewhat, it fixes
our performance issue; tinycc is now compiled in ~8min (WAS: ~1h30).

Next targets:

 - translate mes.c into unsnarfed mes.M2
 - use Gash to remove bash, coreutils&co, grep, sed, tar from the Guix
   bootstrap binaries
 - replace the NixOS bootstrap
 - use dietlibc, uClibc, ... for bootstrapping GNU (bash, binutils,
   gcc, tar) and remove Mes C lib+gnu?
 - bootstrap gcc-3.x or 4.x directly, drop initial gcc-2.95.3 target?
 - have M1+hex2 create gcc/tcc-usable object files?  archives?
 - Debian?
 - ARM, the Hurd?

Packages are available from Guix's core-updates branch.

# About

[GNU Mes](https://www.gnu.org/software/mes0) brings a [Reduced Binary
Seed
bootstrap](http://joyofsource.com/reduced-binary-seed-bootstrap.html)
to [Guix](https://www.gnu.org/software/guix) and potentially to any
other interested GNU/Linux distribution, and aims to help create a
full source bootstrap as part of the [bootstrappable
builds](http://bootstrappable.org) effort.

It consists of a mutual self-hosting Scheme interpreter written in
~5,000 LOC of simple C and a Nyacc-based C compiler written in Scheme.
This mes.c is [being simplified](https://github.com/oriansj/mes-m2) to
be transpiled by [M2-Planet](https://github.com/oriansj/m2-planet).

The Scheme interpreter (mes.c) has a Garbage Collector, a library of
loadable Scheme modules-- notably Dominique Boucher's
[LALR](https://github.com/schemeway/lalr-scm), Pre-R6RS [portable
syntax-case](https://www.cs.indiana.edu/chezscheme/syntax-case/old-psyntax.html)
with R7RS ellipsis, Matt Wette's [Nyacc](https://www.nongnu.org/nyacc)
--and test suite just barely enough to support a simple REPL and
simple C-compiler: MesCC.

Mes+MesCC can compile an only [lightly patched
TinyCC](http://gitlab.com/janneke/tinycc) that is self-hosting.  Using
this tcc and the Mes C library we now have a Reduced Binary Seed
bootstrap for the gnutools triplet: glibc-2.2.5, binutils-2.20.1,
gcc-2.95.3.  This is enough to bootstrap Guix for i686-linux and
x86_64-linux.

Mes is inspired by The Maxwell Equations of Software:
[LISP-1.5](http://www.softwarepreservation.org/projects/LISP/book/LISP%201.5%20Programmers%20Manual.pdf)
-- John McCarthy page 13, GNU Guix's source/binary packaging
transparency and Jeremiah Orians's
[stage0](https://github.com/oriansj/stage0) ~500 byte self-hosting hex
assembler.

# Download

    git clone git://git.savannah.gnu.org/mes.git

  Here are the compressed sources and a GPG detached signature[*]:

    https://ftp.gnu.org/gnu/mes/mes-0.19.tar.gz
    https://ftp.gnu.org/gnu/mes/mes-0.19.tar.gz.sig

  Use a mirror for higher download bandwidth:

    https://www.gnu.org/order/ftp.html

  Here are the MD5 and SHA1 checksums:

    99e134df87adc5fc5fd2c04941929c23  mes-0.19.tar.gz
    c9781b3b6a814acc985c2ac68caa111a56583bca  mes-0.19.tar.gz

  [*] Use a .sig file to verify that the corresponding file (without the
  .sig suffix) is intact.  First, be sure to download both the .sig file
  and the corresponding tarball.  Then, run a command like this:

    gpg --verify mes-0.19.tar.gz.sig

  If that command fails because you don't have the required public key,
  then run this command to import it:

    gpg --keyserver keys.gnupg.net --recv-keys 1A858392E331EAFDB8C27FFBF3C1A0D9C1D65273

  and rerun the 'gpg --verify' command.

Mes runs from the source tree and can also be built, packaged and
installed in Guix[SD] from a git checkout by running

    guix package -f .guix.scm

# Get informed, get involved

    See https://bootstrappable.org
    Join #bootstrappable on irc.freenode.net.

## Changes in 0.19 since 0.18
   * Core
      * The build system has been simplified.
      * Mes now prints a backtrace upon error.
      * Performance has been improved 2-8 times, making Mes 2-10 times slower than Guile.
      * Mes now supports a module type and uses a `boot-module'.
      * Mes now supports a hash_table type.
      * Mes now supports a struct type.
      * Mes now supports building a %bootstrap-mes seed from Guix.
   * Language
      * Records are now implemented using struct (WAS: vector).
      * 44 new functions
 ceil, char-downcase, char-set-adjoin, char-set-complement,
 char-upcase, current-time, delete-file, dup, dup2, file-exists?,
 floor, frame-printer, get-internal-run-time, getcwd, gettimeofday,
 hash, hash-ref, hash-set!, hash-table-printer, hashq,
 hashq-get-handle, hashq-ref, hashq-set, inexact->exact,
 make-hash-table, make-stack, make-struct, module-define!,
 module-printer, module-ref, module-variable, read-line, round,
 stack-length, stack-ref, string-downcase, string-tokenize,
 string-upcase, struct-length, struct-ref, struct-set! struct-vtable,
 struct-vtable, with-error-to-file.
   * MesCC
      * Assembly defines have been cleaned-up: duplicates deleted, missing added, wrong fixed.
      * MesCC now supports compiling GNU Bash and GNU Tar.
         * 6 New functions
 getegid, geteuid, getppid, setgid, setuid, sigdelset, sigprocmask.
          * 22 New macros
 EACCES, ENOSPC, ESPIPE, INT16_MAX, INT16_MIN, INT32_MAX, INT32_MIN,
 INT64_MAX, INT64_MIN, INT8_MAX, INT8_MIN, LLONG_MAX, LLONG_MIN,
 SIZE_MAX SYS_getegid, SYS_geteuid, SYS_setgid SYS_setuid, S_IRGRP,
 S_IROTH, S_IRWXG, S_IRWXO S_ISGID, S_ISUID, S_IWGRP, S_IWOTH, S_IXGRP,
 S_IXOTH, UINT16_MAX, UINT32_MAX, UINT64_MAX, UINT8_MAX,
 _POSIX_VERSION.
   * Noteworthy bug fixes
     * Mes now supports characters #\xNN.
     * Mes now supports assq-ref and assoc-ref with alist == #f.
     * Mes now support \xNN in strings.  Fixes using Nyacc-0.86.0.
     * MesCC now supports the unary plus operator.
     * MesCC now supports the `U' integer suffix.
     * MesCC now comes with INTnn_MIN/MAX, UINTnn defines in stdint.h.
     * MesCC now always exits non-zero when assembler or linker fail.

Greetings,
janneke

  0) [GNU Mes](https://www.gnu.org/software/mes)
  1) [GuixSD](https://www.gnu.org/software/guix)
  2) [bootstrappable builds](http://bootstrappable.org)
  3) [being simplified](https://github.com/oriansj/mes-m2)
  4) [M2-Planet](https://github.com/oriansj/m2-planet)
  5) [LALR](https://github.com/schemeway/lalr-scm)
  6) [portable syntax-case](https://www.cs.indiana.edu/chezscheme/syntax-case/old-psyntax.html)
  7) [Nyacc](https://www.nongnu.org/nyacc)
  8) [bootstrappable TinyCC](https://gitlab.com/janneke/tinycc)
  9) [LISP-1.5](http://www.softwarepreservation.org/projects/LISP/book/LISP%201.5%20Programmers%20Manual.pdf)
 10) [stage0](https://github.com/oriansj/stage0)
