title: We did it!
date: 2021-01-07 08:00
author: Jan (janneke) Nieuwenhuizen
tags: Bootstrapping, Reproducible builds, Trust
slug: we-did-it
---

Last Monday I [sent an
update](https://lists.gnu.org/archive/html/guix-devel/2021-01/msg00036.html)
essentially saying $Subject:

# WE DID IT!!!

A proper blog post will follow once [GNU Mes](https://gnu.org/s/mes)
0.24 has been released and the [Full Source
Bootstrap](https://git.savannah.gnu.org/cgit/guix.git/log/?h=wip-full-source-bootstrap)
has been merged, i.e, when [GNU Guix](https://guix.gnu.org) has a
bootstrap rooted in [this 357-byte
hex0-seed](https://github.com/oriansj/bootstrap-seeds/blob/master/POSIX/x86/hex0-seed)
binary with an
[ASCII-equivalent](https://github.com/oriansj/bootstrap-seeds/blob/master/POSIX/x86/hex0_x86.hex0).

Once released, this is what the bottom of the provisional Guix package
graph will looks like
![gcc-mesboot-bag-graph](images/gcc-core-mesboot0-fsb.png).

At [FOSDEM'21](https://fosdem.org/2021/) I will be [giving a short
talk](https://fosdem.org/2021/schedule/event/gnumes/) in the
[Declarative and Minimalistic Computing
devroom](https://fosdem.org/2021/schedule/track/declarative_and_minimalistic_computing/).

Coverage

  * 2021-01-06: [Linux Weekly News](https://lwn.net/) has a story on [Bootstrappable builds](https://lwn.net/Articles/841797/).


#### About Bootstrappable Builds and GNU Mes

Software is bootstrappable when it does not depend on a binary seed
that cannot be built from source.  Software that is not
bootstrappable---even if it is free software---is a serious security
risk
[for](https://www.ece.cmu.edu/~ganger/712.fall02/papers/p761-thompson.pdf)
[a](https://manishearth.github.io/blog/2016/12/02/reflections-on-rusting-trust/)
[variety](https://www.quora.com/What-is-a-coders-worst-nightmare/answer/Mick-Stute)
[of](http://blog.regehr.org/archives/1241)
[reasons](https://www.alchemistowl.org/pocorgtfo/pocorgtfo08.pdf).
The [Bootstrappable Builds](https://bootstrappable.org/) project aims
to reduce the number and size of binary seeds to a bare minimum.

[GNU Mes](https://www.gnu.org/software/mes/) is closely related to the
Bootstrappable Builds project.  Mes aims to create an entirely
source-based bootstrapping path for the Guix System and other
interested GNU/Linux distributions.  The goal is to start from a
[minimal, easily inspectable
binary](https://github.com/oriansj/bootstrap-seeds/blob/master/POSIX/x86/hex0-seed)
that has an
[ASCII-equivalent](https://github.com/oriansj/bootstrap-seeds/blob/master/POSIX/x86/hex0_x86.hex0)
and bootstrap into something close to R6RS Scheme.

Currently, Mes consists of a mutual self-hosting scheme interpreter
and C compiler.  It also implements a C library.  Mes, the scheme
interpreter, is written in about 5,000 lines of code of simple C or
M2.  It can be bootstrapped using the
[M2-Planet](https://github.com/OriansJ/M2-Planet) transpiler from the
[Stage0 project](https://savannah.gnu.org/projects/stage0).
MesCC, the C compiler, is written in scheme.  Together, Mes and
MesCC can compile [a lightly patched
TinyCC](http://gitlab.com/janneke/tinycc) that is self-hosting.  Using
this TinyCC and the Mes C library, it is possible to bootstrap the
entire Guix System for i686-linux and x86_64-linux.

#### About GNU Guix

[GNU Guix](https://guix.gnu.org) is a transactional package
manager and an advanced distribution of the GNU system that [respects
user
freedom](https://www.gnu.org/distros/free-system-distribution-guidelines.html).
Guix can be used on top of any system running the kernel Linux, or it
can be used as a standalone operating system distribution for i686,
x86_64, ARMv7, and AArch64 machines.

In addition to standard package management features, Guix supports
transactional upgrades and roll-backs, unprivileged package management,
per-user profiles, and garbage collection.  When used as a standalone
GNU/Linux distribution, Guix offers a declarative, stateless approach to
operating system configuration management.  Guix is highly customizable
and hackable through [Guile](https://www.gnu.org/software/guile)
programming interfaces and extensions to the
[Scheme](http://schemers.org) language.
