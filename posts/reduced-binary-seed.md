title: Reduced Binary Seed Bootstrap
date: 2018-09-19 17:23:09
tags: mes
summary: Reduced Binary Seed Bootstrap
---

Guix---like other GNU/Linux distributions---is traditionally
bootstrapped from a set of bootstrap binaries: Bourne shell,
command-line tools provided by GNU Coreutils, Awk, Findutils, `sed`,
and `grep` and Guile, GCC, Binutils, and the GNU C Library.  Usually,
these bootstrap binaries are "taken for granted."

Taking these binaries for granted means that we consider them to be a
correct and trustworthy `seed` for building the complete system.
Therein lies a problem: the current combined size of these bootstrap
binaries is about 250MB .  Auditing or even inspecting these is next
to impossible.

For `i686-linux` and `x86_64-linux`, GuixSD now features a "Reduced
Binary Seed" bootstrap (see the
[`wip-bootstrap`](http://git.savannah.gnu.org/cgit/guix.git/log/?h=wip-bootstrap)
branch).  We would like to say: "Full Source Bootstrap" and while we
are working towards that goal it would be hyperbole to use that term
for what we do now.

The Reduced Binary Seed bootstrap removes the most critical tools---from a
trust perspective---from the bootstrap binaries: GCC, Binutils and the GNU C
Library are replaced by: `mescc-tools-seed` (a tiny assembler and linker)
`mes-seed` (a small Scheme Interpreter and a C compiler writen in Scheme)
and `tinycc-seed` (the Mes C Library, built for TinyCC).  Using these new
binary seeds and a new set of
packages
(mescc-tools-boot,
nyacc-boot,
mes-boot,
tcc-boot0,
tcc-boot,
make-mesboot0,
diffutils-mesboot,
binutils-mesboot0,
gcc-core-mesboot,
mesboot-headers,
glibc-mesboot0,
gcc-mesboot0,
binutils-mesboot,
make-mesboot,
gcc-mesboot1,
gcc-mesboot1-wrapper,
glibc-headers-mesboot,
glibc-mesboot,
gcc-mesboot,
and
gcc-mesboot-wrapper)
the "missing" Binutils, GCC, and the GNU C Library are built from source.
From here on the more traditional bootstrap process resumes.  This approach
has reduced the bootstrap binaries in size to about 130MB.  Work is ongoing to
reduce this further.  If you are interested, join us on `#bootstrappable`
on the Freenode IRC network.

Below is the generated dependency graph for `gcc-mesboot`, the bootstrap
compiler used to build the rest of GuixSD.

![gcc-mesboot-bag-graph](images/gcc-mesboot-bag-graph.png)


