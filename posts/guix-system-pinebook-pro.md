title: Guix System on the Pinebook Pro
date: 2020-02-15 00:00:00 +0100
author: janneke
tags: ARM, pinebook-pro
summary: Installing Guix System on the Pinebook Pro
---

![pinebook pro with exwm](images/pinebook-pro.png)

The new Pinebook Pro is a very lovable machine.  For only $200 excluding
shipping (and possibly taxes, I paid $68) the keyboard feels different
but comparable to a flat [Dell XPS 13
9350](https://www.dell.com/en-us/work/shop/dell-laptops-and-notebooks/new-xps-13-developer-edition/spd/xps-13-9350-laptop-ubuntu).
The screen is beautiful and the machine is remarkably silent.  So much
that at times you can hear the display almost like in the old days.

I have been waiting for an affordable ARM with more than 2GiB of
memory to help out with the ARM port of [GNU
Mes](https://www.gnu.org/software/mes).  Although the Qemu-service in
Guix works neat, having the actual hardware helps a lot.

It does feel slow---even although it has 6 cores---so it may take some
time before we can really leave proprietary MEs and BIOSes behind.
Also, the Hurd does not run on ARM/AArch64 yet.  Bogomips are at 48
per CPU compared to 4400 for an i7, but it is certainly not two orders
slower.  Is it?  Compiling seems to be about 4x slower than the i7.

I got some inspiration from the lovely post on [Guix on an ARM
board](https://guix.gnu.org/blog/2019/guix-on-an-arm-board/) by Julien
Lepiller and had lots of help from Danny Milosavljevic during the
[Guix Days](https://guix.gnu.org/blog/2020/meet-guix-at-fosdem-2020/)
at [Fosdem20](https://fosdem.org).

In a [previous
post](a-bare-bones-guix-system-on-the-pinebook-pro.html) I described
how I hacked to boot into a [GNU Guix](https://guix.gnu.org) root on a
[Pinebook Pro](https://pine64.org/pinebook-pro), using the default
kernel provided with the [Debian
image](https://github.com/mrfixit2001/debian_desktop/releases/download/191123/pinebookpro-debian-desktop-mrfixit-191123.img.xz).

For the Guix System, I have packaged and
[deblobbed](https://lilypond.org/janneke/pinebook-pro/deblob-pinebook-pro-5.5)
the [Manjaro
kernel](https://gitlab.manjaro.org/tsys/linux-pinebook-pro), which
seems to be where development of mainlining the Pinebook Pro kernel
has moved.

We now have a recipe to install a true Guix System with [Linux
Libre](https://linux-libre.fsfla.org).  Also, I have added a u-boot
package variants: we no longer depend on 3rd party binary images.

We start by *infecting* our beautiful Debian system with the Guix
package manager

```
sudo apt install gpg
wget https://sv.gnu.org/people/viewgpg.php?user_id=15145 -qO - | gpg --import -
wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
sudo bash guix-install.sh
```

First, insert the microSD, `tail -f /var/log/messages` should say
something like

```
Feb 15 14:33:01 armzalig kernel: [ 4150.344615] mmc0: new ultra high speed SDR104 SDXC card at address 0001
Feb 15 14:33:01 armzalig kernel: [ 4150.345537] mmcblk0: mmc0:0001 SD16G 58.9 GiB 
Feb 15 14:33:01 armzalig kernel: [ 4150.346755]  mmcblk0:
```

Then, add a bootable partition and create a file system (please verify
the correctness of /dev/mmc\* for your device!)

```
sudo parted -- /dev/mmcblk0 mkpart p 0% -1
sudo parted -- /dev/mmcblk0 set 1 boot on
sudo mkfs.ext4 -F /dev/mmcblk0p1
```

We need to upgrade Guix to a more recent version.  At the time of
writing we need to avoid [bug #39352](https://debbugs.gnu.org/39352)
and pull to [`c7dd15596ffd09ab40629c89e7014e51a4d7e95e`](http://git.savannah.gnu.org/cgit/guix.git/commit/?id=c7dd15596ffd09ab40629c89e7014e51a4d7e95e)

```
guix pull --commit=c7dd15596ffd09ab40629c89e7014e51a4d7e95e

```

After running that, it told me to do

```
export PATH="/home/janneke/.config/guix/current/bin${PATH:+:}$PATH"
hash guix

```

which I did, that may be a bit different for you.


```
guix environment guix --ad-hoc git
#git clone https://git.savannah.gnu.org/git/guix.git  # not yet
git clone https://gitlab.com/janneke/guix.git
cd guix
git checkout wip-pinebook-pro
./bootstrap
./configure --localstatedir=/var GUILE=$(type -p guile) GUILD=$(type -p guild)
make
```

I copied [pine-book-pro.tmpl@savannah](https://git.savannah.gnu.org/cgit/guix.git/plain/gnu/system/examples/pinebook-pro.tmpl?h=wip-pinebook-pro)/[pine-book-pro.tmpl@gitlab](https://gitlab.com/janneke/guix/-/blob/wip-pinebook-pro/gnu/system/examples/pinebook-pro.tmpl) into [arm-baar.scm](../images/arm-baar.scm)

```scheme
;; This is an operating system configuration template
;; for a "bare bones" pinebook pro

;; The default image's kernel 4.4.190 has
;;    microSD: /dev/mmcblk0
;;    eMMC: /dev/mmcblk1
;;
;; Note that after booting the Guix System with linux-libre
;; 5.5-pinebook-pro this changes to
;;    microSD: /dev/mmcblk1
;;    eMMC: /dev/mmcblk2

;; Assuming https://gitlab.com/janneke/guix.git wip-pinebook-pro
;; has been built in .
;; cp gnu/system/examples/pinebook-pro.tmpl arm-baar.scm
;; sudo -E ./pre-inst-env guix system init arm-baar.scm /mnt --fallback

(use-modules (gnu) (gnu bootloader u-boot))
(use-service-modules avahi networking ssh)
(use-package-modules admin bootloaders certs linux ssh)

(operating-system
  (host-name "armzalig")
  (timezone "Europe/Amsterdam")
  (locale "en_US.utf8")

  ;; Assuming not using a typewriter that needs qwerty slowdown
  (keyboard-layout (keyboard-layout "us" "dvorak"
                                    #:options '("ctrl:nocaps" "compose:menu")))

  ;; Assuming /dev/mmcblk0 is the microSD...
  (bootloader (bootloader-configuration
               (target "/dev/mmcblk0")
               (bootloader u-boot-pinebook-pro-rk3399-bootloader)))
  ;; ...and after booting, /dev/mmcblk1p1 is the root file system
  (file-systems (cons* (file-system (device "/dev/mmcblk1p1")
                                    (mount-point "/")
                                    (type "ext4"))
                       %base-file-systems))

  (kernel linux-libre-pinebook-pro)
  (kernel-arguments '("ethaddr=${ethaddr}" "eth1addr=${eth1addr}" "serial=${serial#}"
                      "video=HDMI-A-1:1920x1080@60" "video=eDP-1:1920x1080@60"
                      "vga=current"))
  (initrd-modules '())

  (users (cons* (user-account (name "guix")
                              (group "users")
                              (supplementary-groups '("wheel")))
                %base-user-accounts))
  (name-service-switch %mdns-host-lookup-nss)
  (packages (cons* nss-certs openssh wpa-supplicant-minimal %base-packages))
  (services (cons* (service dhcp-client-service-type)
                   (service openssh-service-type
                            (openssh-configuration
                             (port-number 2222)))
                   %base-services)))
```

Save it as `arm-baar.scm`, choose your hostname, timezone, locale and
keyboard layout.  After mounting the root file system

```
sudo mount /dev/mmcblk0p1 /mnt
```

install Guix onto it, doing
 
```
sudo -E ./pre-inst-env guix system init arm-baar.scm /mnt --fallback
```

says something like


```
The following derivations will be built:
   /gnu/store/7pad9i05ay9kmayh8l8i8r6wpir88f5g-system.drv
   /gnu/store/0rkjcqhkb30cban0291mqbv2yfi02n0f-boot.drv
   /gnu/store/vrfajn06i669wh27c2zia9wah7k1j3ws-activate.scm.drv
   /gnu/store/w1qchgxmvrsysd7q1r8h7izxb18r33g6-activate-service.scm.drv
   /gnu/store/g5awvk155l1nygd7mbg947pgcxx3cp3c-etc.drv
   /gnu/store/md0p8hcnfq2pf43wa51ajsapky53a1jm-provenance.drv
   /gnu/store/l2g009sdbm3x5qajnfgjfn4rj04p4l58-extlinux.conf.drv
building /gnu/store/md0p8hcnfq2pf43wa51ajsapky53a1jm-provenance.drv...
building /gnu/store/g5awvk155l1nygd7mbg947pgcxx3cp3c-etc.drv...
building /gnu/store/w1qchgxmvrsysd7q1r8h7izxb18r33g6-activate-service.scm.drv...
building /gnu/store/vrfajn06i669wh27c2zia9wah7k1j3ws-activate.scm.drv...
building /gnu/store/0rkjcqhkb30cban0291mqbv2yfi02n0f-boot.drv...
building /gnu/store/7pad9i05ay9kmayh8l8i8r6wpir88f5g-system.drv...
building /gnu/store/l2g009sdbm3x5qajnfgjfn4rj04p4l58-extlinux.conf.drv...
/gnu/store/bpq3gjfih8c1xgahw7yj3h18d1sw670l-system
/gnu/store/hgk8ya40n3pppp7sja12cdr6wkb3k0nr-extlinux.conf

initializing operating system under '/mnt'...
copying to '/mnt'...
populating '/mnt'...
substitute: updating substitutes from 'https://ci.guix.gnu.org'... 100.0%
substitute: updating substitutes from 'http://kluit.dezyne.org:8181'... 100.0%
substitute: updating substitutes from 'https://ci.guix.gnu.org'... 100.0%
substitute: updating substitutes from 'http://kluit.dezyne.org:8181'... 100.0%
downloading from https://ci.guix.gnu.org/nar/gzip/0dqzh840z4a8a23gvwc38ssmxb9qfcc2-u-boot-2020.01-pinebook-pro-1.365495a-checkout...
 u-boot-2020.01-pinebook-pro-1.365495a-checkout            1.8MiB/s 00:11 | 20.4MiB transferred
building /gnu/store/djgdaps803zizw77y9jgsg4m0dl06aik-u-boot-pinebook-pro-rk3399-2020.01-pinebook-pro-1.365495a.drv...
substitute: updating substitutes from 'https://ci.guix.gnu.org'... 100.0%
building /gnu/store/p97hj16a738kg64f5b87s2g54d5qmc3s-module-import-compiled.drv...
building /gnu/store/6r1xrpk1d17scaxpv7fzv1dxany9fafz-install-bootloader.scm.drv...
guix system: bootloader successfully installed on '/dev/mmcblk0'

```

Weirdly, we need to fixup `extlinux.conf`.  Although the default

```
$ grep DEFAULT_FDT /gnu/store/zxb488q5l9c6j5pnazvp5s30z1p18gnm-u-boot-pinebook-pro-rk3399-2020.01-pinebook-pro-1.365495a/libexec/.config 
CONFIG_DEFAULT_FDT_FILE="rockchip/rk3399-pinebook-pro.dtb"
```

seems to be correct, upon boot u-boot wants to load `rockchip-evb_rk3399.dtb`

```
** File not found /gnu/store/4zsyqb40r34hp8q83fs5q3kpw4djs046-linux-libre-pinebook-pro-5.5.0/lib/dtbs/rockchip-evb_rk3399.dtb **
```

Oh, well

```
sudo sed 's,FDTDIR \([^ ]*\),FDT \1/rockchip/rk3399-pinebook-pro.dtb,' /mnt/boot/extlinux/extlinux.conf
```

And that's it, good luck!

```
sudo umount /mnt
sudo reboot
```

And there you have it

![pinebook pro with guix](images/pinebook-pro-guix.png)

no WIFI or accelerated X yet; but you gotta love `This is the GNU system.  Welcome.`

What really got me going here was a serial console.  The trick is to
get [a working
cable](https://www.allekabels.nl/jack-kabel/4/1193169/jack-naar-usb-kabel.html),
remove the backcover

![pinebook pro with exwm](images/pinebook-pro-open.png)

and [flip the switch #9 Audio/UART upward](https://wiki.pine64.org/index.php/Pinebook_Pro#Mainboard_Switches_and_Buttons)
![pinebook pro with exwm](images/pinebook-pro-switch.png)

I first got one that did not work.  When you plug-in a working cable
in the client, `/var/log/messages` should show something like

```
Feb 15 15:03:55 localhost vmunix: [113105.608514] usb 1-1: new full-speed USB device number 8 using xhci_hcd
Feb 15 15:03:55 localhost vmunix: [113105.762159] usb 1-1: New USB device found, idVendor=0403, idProduct=6001, bcdDevice= 6.00
Feb 15 15:03:55 localhost vmunix: [113105.762161] usb 1-1: New USB device strings: Mfr=1, Product=2, SerialNumber=3
Feb 15 15:03:55 localhost vmunix: [113105.762162] usb 1-1: Product: FT232R USB UART
Feb 15 15:03:55 localhost vmunix: [113105.762163] usb 1-1: Manufacturer: FTDI
Feb 15 15:03:55 localhost vmunix: [113105.762164] usb 1-1: SerialNumber: AC01OG7W
Feb 15 15:03:55 localhost vmunix: [113105.765223] ftdi_sio 1-1:1.0: FTDI USB Serial Device converter detected
Feb 15 15:03:55 localhost vmunix: [113105.765246] usb 1-1: Detected FT232RL
Feb 15 15:03:55 localhost vmunix: [113105.765527] usb 1-1: FTDI USB Serial Device converter now attached to ttyUSB0
```

telling you that `ttyUSB0` is now available.  Then you can connect using `screen`

```
screen /dev/ttyUSB0 1500000
```

that even works before plugging it into the Pinebook.  After plugging
in the earphone jack and rebooting the Pinebook, you can follow,
interact and debug the boot process.

#### About GNU Guix

[GNU Guix](https://guix.gnu.org) is a transactional package
manager and an advanced distribution of the GNU system that [respects
user
freedom](https://www.gnu.org/distros/free-system-distribution-guidelines.html).
Guix can be used on top of any system running the kernel Linux, or it
can be used as a standalone operating system distribution for i686,
x86_64, ARMv7, and AArch64 machines.

In addition to standard package management features, Guix supports
transactional upgrades and roll-backs, unprivileged package management,
per-user profiles, and garbage collection.  When used as a standalone
GNU/Linux distribution, Guix offers a declarative, stateless approach to
operating system configuration management.  Guix is highly customizable
and hackable through [Guile](https://www.gnu.org/software/guile)
programming interfaces and extensions to the
[Scheme](http://schemers.org) language.

#### About Pinebook Pro

[PINEBOOK Pro](https://pine64.org/pinebook-pro)

A Powerful, Metal and Open Source ARM 64-Bit Laptop for Work, School
or Fun The Pinebook Pro is meant to deliver solid day-to-day Linux or
\*BSD experience and to be a compelling alternative to mid-ranged
Chromebooks that people convert into Linux laptops. In contrast to
most mid-ranged Chromebooks however, the Pinebook Pro comes with an
IPS 1080p 14″ LCD panel, a premium magnesium alloy shell, 64/128GB of
eMMC storage, a 10,000 mAh capacity battery and the modularity /
hackability that only an open source project can deliver – such as the
unpopulated PCIe m.2 NVMe slot (an optional feature which requires an
optional adapter). The USB-C port on the Pinebook Pro, apart from
being able to transmit data and charge the unit, is also capable of
digital video output up-to 4K at 60hz.

[0]: A difficulty here is that the Pinebook Pro currently does not
want to show a U-Boot prompt, command line or debug output.  If you
consider to buy one, you may want to also order a serial cable that
might help debugging the boot sequence.
