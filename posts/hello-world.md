title: Hello, world!
date: 2018-09-23 00:00:00
tags: hello
summary: My website is Haunted!
---

Finally, my website has been [Haunted](http://haunt.dthompson.us)!

![haunt logo](images/haunt/logo.png)

Connect with me on [#guix @freenode](irc://freenode.net/#guix),
[#bootstrappable @freenode](irc://freenode.net/#bootstrappable),
[mastodon](https://octodon.social/@janneke) or
[twitter](https://twitter.com/janneke_gnu).

Update: the date of this post has been changed from
[EPOCH](https://en.wikipedia.org/wiki/Epoch) as meaning "first post"
to the arbitrary git commit date because some "modern" softwares
cannot handle that.
