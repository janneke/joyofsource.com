title: A Bare bones Guix System on the Pinebook Pro
date: 2020-02-09 00:00:00 +0100
author: janneke
tags: ARM, pinebook
summary: Cheats to boot Guix System on PBP
---

Below is the recipe of how I hacked to boot into a [GNU
Guix](https://guix.gnu.org) root on a [Pinebook
Pro](https://pine64.org/pinebook-pro), using the default kernel
provided with the [Debian
image](https://github.com/mrfixit2001/debian_desktop/releases/download/191123/pinebookpro-debian-desktop-mrfixit-191123.img.xz).

![pinebook pro with exwm](images/pinebook-pro.png)

The new Pinebook Pro is a very lovable machine.  For only $200 excluding
shipping (and possibly taxes, I paid $68) the keyboard feels different
but comparable to a flat [Dell XPS 13
9350](https://www.dell.com/en-us/work/shop/dell-laptops-and-notebooks/new-xps-13-developer-edition/spd/xps-13-9350-laptop-ubuntu).
The screen is beautiful and the machine is remarkably silent.  So much
that at times you can hear the display almost like in the old days.

I have been waiting for an affordable ARM with more than 2GiB of
memory to help out with the ARM port of [GNU
Mes](https://www.gnu.org/software/mes).  Although the Qemu-service in
Guix works neat, having the actual hardware helps a lot.

The default installation uses a GNOME-2 (Mate) desktop but that's
easily replaced by [Exwm](https://github.com/ch11ng/exwm) which works
nicely.  Disabling the touchpad is straightforward using `xinput`.

It does feel slow---even although it has 6 cores---so it may take some
time before we can really leave proprietary MEs and BIOSes behind.
Also, the Hurd does not run on ARM/AArch64 yet.  Bogomips are at 48
per CPU compared to 4400 for an i7, but it is certainly not two orders
slower.  Is it?  Compiling seems to be about 4x slower than the i7.

I got some inspiration from the lovely post on [Guix on an ARM
board](https://guix.gnu.org/blog/2019/guix-on-an-arm-board/) by Julien
Lepiller and had lots of help from Danny Milosavljevic during the
[Guix Days](https://guix.gnu.org/blog/2020/meet-guix-at-fosdem-2020/)
at [Fosdem20](https://fosdem.org).

We start by *infecting* our beautiful Debian system with the Guix
package manager

```
sudo apt install gpg
wget https://sv.gnu.org/people/viewgpg.php?user_id=15145 -qO - | gpg --import -
wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
sudo bash guix-install.sh
```

Installing das U-Boot, or building a patched Linux-Libre kernel has
not been succesfully attempted yet[0].  We start by piggybacking off
the official image.  Download it, and write it to the MMC (please
verify the correctness of /dev/mmc\* for your device)

```
wget https://github.com/mrfixit2001/debian_desktop/releases/download/191123/pinebookpro-debian-desktop-mrfixit-191123.img.xz
xz -dc pinebookpro-debian-desktop-mrfixit-191123.img.xz > debian.img
sudo dd if=debian.img of=/dev/mmcblk0 status=progress
```

this should output something like

```
5376238080 bytes (5.4 GB, 5.0 GiB) copied, 1055 s, 5.1 MB/s
10502144+0 records in
10502144+0 records out
5377097728 bytes (5.4 GB, 5.0 GiB) copied, 1066.63 s, 5.0 MB/s
```

If you haven't done so before then it's probably a good idea to try
booting from that card, to see if it works for you.

Then, resize root and wipe it

```
sudo parted -- /dev/mmcblk0 resizepart 2 -1
sudo mkfs.ext4 -F /dev/mmcblk0p2
```

We need to upgrade Guix to a more recent version.  At the time of
writing we need to avoid [bug #39352](https://debbugs.gnu.org/39352)
and pull to [`c7dd15596ffd09ab40629c89e7014e51a4d7e95e`](http://git.savannah.gnu.org/cgit/guix.git/commit/?id=c7dd15596ffd09ab40629c89e7014e51a4d7e95e)

```
guix pull --commit=c7dd15596ffd09ab40629c89e7014e51a4d7e95e

```

After running that, it told me to do

```
export PATH="/home/janneke/.config/guix/current/bin${PATH:+:}$PATH"
hash guix

```

which I did, that may be a bit different for you.

I changed [bare-bones.tmpl](https://git.savannah.gnu.org/cgit/guix.git/plain/gnu/system/examples/bare-bones.tmpl) into [arm-baar0.scm](../images/arm-baar0.scm)

```
;; This is an operating system configuration template
;; for a "bare bones" pinebook pro, with no X11 display server.
(use-modules (gnu) (gnu bootloader u-boot))
(use-service-modules networking ssh)
(use-package-modules admin bootloaders linux ssh)

(operating-system
  (host-name "armzalig")
  (timezone "Europe/Amsterdam")
  (locale "en_US.utf8")
  ;; Your preference here
  (keyboard-layout (keyboard-layout "us" "dvorak"
                                    #:options '("ctrl:nocaps" "compose:menu")))
  (bootloader (bootloader-configuration
               (target "/dev/mmcblk0")
               ;; `u-boot-bootloader' is a dummy: no embedding will happen
               (bootloader u-boot-bootloader)))
  (kernel linux-libre-4.4)
  (kernel-arguments '("ethaddr=${ethaddr}" "eth1addr=${eth1addr}" "serial=${serial#}"
                      "video=HDMI-A-1:1920x1080@60" "video=eDP-1:1920x1080@60"
                      "vga=current"))
  (initrd-modules '())
  (file-systems (cons* (file-system (device "/dev/mmcblk0p2")
                                    (mount-point "/")
                                    (type "ext4"))
                       %base-file-systems))
  (users (cons* (user-account (name "guix")
                              (group "users")
                              (supplementary-groups '("wheel")))
                %base-user-accounts))
         (packages (cons* openssh wpa-supplicant-minimal %base-packages))
         (services (cons* (service dhcp-client-service-type)
                          (service openssh-service-type
                                   (openssh-configuration
                                    (port-number 2222)))
                          %base-services)))
```

We are using the dummy `u-boot-bootloader'; this will not attempt to
install any bootloader.  For now that's "great", we will just be using
the one on the card that already works.

Save it as `arm-baar0.scm`, choose your hostname, timezone, locale and
keyboard layout.  After mounting root and boot

```
sudo mount /dev/mmcblk0p2 /mnt
sudo mkdir /mnt/boot
sudo mount /dev/mmcblk0p1 /mnt/boot
```

use it to install Guix
 
```
sudo -E $(type -p guix) system init arm-baar0.scm /mnt
```

And now for the ugly hack.  The mainline linux-libre-5.4 kernel does
not build for me and the linux-libre-4.4 kernel build but does not
boot[^1].  So, we mangle the U-Boot configuration file to use the
stock kernel


```
sudo sed -i 's,KERNEL.*,KERNEL /Image,' /mnt/boot/extlinux/extlinux.conf
sudo sed -i 's,FDTDIR.*,FDT /rk3399-pinebookpro.dtb,' /mnt/boot/extlinux/extlinux.conf
sudo mkdir -p /mnt/boot/gnu/store/$(grep -o [^/]*-raw-initrd /mnt/boot/extlinux/extlinux.conf)
sudo cp /mnt/gnu/store/$(grep -o [^/]*-raw-initrd/initrd.cpio.gz /mnt/boot/extlinux/extlinux.conf) \
  /mnt/boot/gnu/store/$(grep -o [^/]*-raw-initrd /mnt/boot/extlinux/extlinux.conf)
```

which results in

```
# This file was generated from your Guix configuration.  Any changes
# will be lost upon reconfiguration.
UI menu.c32
MENU TITLE GNU Guix Boot Options
PROMPT 1
TIMEOUT 50
LABEL GNU with Linux-Libre-Pinebook-Pro 5.5.0
  MENU LABEL GNU with Linux-Libre-Pinebook-Pro 5.5.0
  KERNEL /Image
  FDT /rk3399-pinebookpro.dtb
  INITRD /gnu/store/nz9agd7dhzmm81vqhymi4h36hvcb7qri-raw-initrd/initrd.cpio.gz
  APPEND --root=/dev/mmcblk0p2 --system=/gnu/store/8s856m8cbgiw9yhmy20sqsvxza0c2w44-system --load=/gnu/store/8s856m8cbgiw9yhmy20sqsvxza0c2w44-system/boot ethaddr=${ethaddr} eth1addr=${eth1addr} serial=${serial#} video=HDMI-A-1:1920x1080@60 video=eDP-1:1920x1080@60 vga=current
```

And that's it, good luck!

```
sudo umount /mnt/boot
sudo umount /mnt
sudo reboot
```

And there you have it

![pinebook pro with guix](images/pinebook-pro-guix.png)

no wifi or X yet; but you gotta love `This is the GNU system.  Welcome.`

I have been looking into building and deblobbing [the Manjaro
kernel](https://gitlab.manjaro.org/tsys/linux-pinebook-pro), which
seems to be where development of mainlining the Pinebook Pro kernel
has moved.  While it builds, it does not boot for me yet.  I'm sure,
in time it will.  Once I get u-boot debugging to work, either over a
serial line or by running qemu, I'll update this post.

`Open.  Friendly.  Community driven.`

#### About GNU Guix

[GNU Guix](https://guix.gnu.org) is a transactional package
manager and an advanced distribution of the GNU system that [respects
user
freedom](https://www.gnu.org/distros/free-system-distribution-guidelines.html).
Guix can be used on top of any system running the kernel Linux, or it
can be used as a standalone operating system distribution for i686,
x86_64, ARMv7, and AArch64 machines.

In addition to standard package management features, Guix supports
transactional upgrades and roll-backs, unprivileged package management,
per-user profiles, and garbage collection.  When used as a standalone
GNU/Linux distribution, Guix offers a declarative, stateless approach to
operating system configuration management.  Guix is highly customizable
and hackable through [Guile](https://www.gnu.org/software/guile)
programming interfaces and extensions to the
[Scheme](http://schemers.org) language.

#### About Pinebook Pro

[PINEBOOK Pro](https://pine64.org/pinebook-pro)

A Powerful, Metal and Open Source ARM 64-Bit Laptop for Work, School
or Fun The Pinebook Pro is meant to deliver solid day-to-day Linux or
\*BSD experience and to be a compelling alternative to mid-ranged
Chromebooks that people convert into Linux laptops. In contrast to
most mid-ranged Chromebooks however, the Pinebook Pro comes with an
IPS 1080p 14″ LCD panel, a premium magnesium alloy shell, 64/128GB of
eMMC storage, a 10,000 mAh capacity battery and the modularity /
hackability that only an open source project can deliver – such as the
unpopulated PCIe m.2 NVMe slot (an optional feature which requires an
optional adapter). The USB-C port on the Pinebook Pro, apart from
being able to transmit data and charge the unit, is also capable of
digital video output up-to 4K at 60hz.

[0]: A difficulty here is that the Pinebook Pro currently does not
want to show a U-Boot prompt, command line or debug output.  If you
consider to buy one, you may want to also order a serial cable that
might help debugging the boot sequence.
