;;; .guix.scm -- Guix package definition

;;; Copyright © 2018 David Thompson <davet@gnu.org>
;;; Copyright © 2018,2022 <janneke@gnu.org>
;;;
;;; This file is part of joyofsource.com.
;;;
;;; joyofsource.com is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; joyofsource.com is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with joyofsource.com.  If not, see <http://www.gnu.org/licenses/>.

(use-modules (guix profiles)
             (gnu packages base)
             (gnu packages guile)
             (gnu packages guile-xyz)
             (gnu packages package-management)
             (gnu packages rsync)
             (gnu packages tls)
             (gnu packages version-control))

(packages->manifest
 (list git
       glibc
       glibc-locales
       gnutls
       guile-3.0
       guile-json-3
       guile-syntax-highlight
       guix
       haunt
       gnu-make
       rsync))
