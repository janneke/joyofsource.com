# .guix.scm -- Guix package definition

# joyofsource.com -- janneke's website.
# Copyright © 2018,2022 <janneke@gnu.org>
#
# This file is part of joyofsource.com.
#
# joyofsource.com is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or (at
# your option) any later version.
#
# joyofsource.com is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with joyofsource.com.  If not, see <http://www.gnu.org/licenses/>.

SITE:=joyofsource
CONFIG:=$(SITE).scm
$(info CONFIG=$(CONFIG))

.PHONY: all clean default haunt serve gnu-dot-org

default: all

all: haunt

clean:
	git clean -fdx

haunt:
	haunt build --config=$(CONFIG)

serve:
	haunt build --config=$(CONFIG)
	haunt serve --config=$(CONFIG) --watch --port=8888

publish: all
	rsync -P -rvz --delete site/ shell.dds.nl:public_html/$(SITE)
