;; This is an operating system configuration template
;; for a "bare bones" pinebook pro

;; The default image's kernel 4.4.190 has
;;    microSD: /dev/mmcblk0
;;    eMMC: /dev/mmcblk1
;;
;; Note that after booting the Guix System with linux-libre
;; 5.5-pinebook-pro this changes to
;;    microSD: /dev/mmcblk1
;;    eMMC: /dev/mmcblk2

;; Assuming https://gitlab.com/janneke/guix.git wip-pinebook-pro
;; has been built in .
;; cp gnu/system/examples/pinebook-pro.tmpl arm-baar.scm
;; sudo -E ./pre-inst-env guix system init arm-baar.scm /mnt --fallback

(use-modules (gnu) (gnu bootloader u-boot))
(use-service-modules avahi networking ssh)
(use-package-modules admin bootloaders certs linux ssh)

(operating-system
  (host-name "armzalig")
  (timezone "Europe/Amsterdam")
  (locale "en_US.utf8")

  ;; Assuming not using a typewriter that needs qwerty slowdown
  (keyboard-layout (keyboard-layout "us" "dvorak"
                                    #:options '("ctrl:nocaps" "compose:menu")))

  ;; Assuming /dev/mmcblk0 is the microSD...
  (bootloader (bootloader-configuration
               (target "/dev/mmcblk0")
               (bootloader u-boot-pinebook-pro-rk3399-bootloader)))
  ;; ...and after booting, /dev/mmcblk1p1 is the root file system
  (file-systems (cons* (file-system (device "/dev/mmcblk1p1")
                                    (mount-point "/")
                                    (type "ext4"))
                       %base-file-systems))

  (kernel linux-libre-pinebook-pro)
  (kernel-arguments '("ethaddr=${ethaddr}" "eth1addr=${eth1addr}" "serial=${serial#}"
                      "video=HDMI-A-1:1920x1080@60" "video=eDP-1:1920x1080@60"
                      "vga=current"))
  (initrd-modules '())

  (users (cons* (user-account (name "guix")
                              (group "users")
                              (supplementary-groups '("wheel")))
                %base-user-accounts))
  (name-service-switch %mdns-host-lookup-nss)
  (packages (cons* nss-certs openssh wpa-supplicant-minimal %base-packages))
  (services (cons* (service dhcp-client-service-type)
                   (service openssh-service-type
                            (openssh-configuration
                             (port-number 2222)))
                   %base-services)))
