;; This is an operating system configuration template
;; for a "bare bones" pinebook pro, with no X11 display server.
(use-modules (gnu) (gnu bootloader u-boot))
(use-service-modules networking ssh)
(use-package-modules admin bootloaders linux ssh)

(operating-system
  (host-name "armzalig")
  (timezone "Europe/Amsterdam")
  (locale "en_US.utf8")
  ;; Your preference here
  (keyboard-layout (keyboard-layout "us" "dvorak"
                                    #:options '("ctrl:nocaps" "compose:menu")))
  (bootloader (bootloader-configuration
               (target "/dev/mmcblk0")
               ;; `u-boot-bootloader' is a dummy: no embedding will happen
               (bootloader u-boot-bootloader)))
  (kernel linux-libre-4.4)
  (kernel-arguments '("ethaddr=${ethaddr}" "eth1addr=${eth1addr}" "serial=${serial#}"
                      "video=HDMI-A-1:1920x1080@60" "video=eDP-1:1920x1080@60"
                      "vga=current"))
  (initrd-modules '())
  (file-systems (cons* (file-system (device "/dev/mmcblk0p2")
                                    (mount-point "/")
                                    (type "ext4"))
                       %base-file-systems))
  (users (cons* (user-account (name "guix")
                              (group "users")
                              (supplementary-groups '("wheel")))
                %base-user-accounts))
         (packages (cons* openssh wpa-supplicant-minimal %base-packages))
         (services (cons* (service dhcp-client-service-type)
                          (service openssh-service-type
                                   (openssh-configuration
                                    (port-number 2222)))
                          %base-services)))
