;;; Copyright © 2018 David Thompson <davet@gnu.org>
;;; Copyright © 2018,2019,2022,2023 Janneke Nieuwenhuizen <janneke@gnu.org>
;;;
;;; This file is part of joyofsource.com.
;;;
;;; joyofsource.com is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; joyofsource.com is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with joyofsource.com.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; The text and images on this site are free culture works available
;;; under the Creative Commons Attribution-NoDerivs 4.0 license, see
;;; https://creativecommons.org/licenses/by-nd/4.0/

(use-modules (haunt asset)
             (haunt builder blog)
             (haunt builder atom)
             (haunt builder assets)
             (haunt html)
             (haunt page)
             (haunt post)
             (haunt reader)
             (haunt reader commonmark)
             (haunt reader texinfo)
             (haunt site)
             (haunt utils)
             (commonmark)
             (syntax-highlight)
             (syntax-highlight scheme)
             (syntax-highlight xml)
             (syntax-highlight c)
             (sxml match)
             (sxml transform)
             (texinfo)
             (texinfo html)
             (srfi srfi-1)
             (srfi srfi-19)
             (ice-9 rdelim)
             (ice-9 regex)
             (ice-9 match)
             (web uri))

(define (date year month day)
  "Create a SRFI-19 date for the given YEAR, MONTH, DAY"
  (let ((tzoffset (tm:gmtoff (localtime (time-second (current-time))))))
    (make-date 0 0 0 0 day month year tzoffset)))

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(string-append "/css/" name ".css")))))

(define* (anchor content #:optional (uri content))
  `(a (@ (href ,uri)) ,content))

(define %cc-by-sa-link
  '(a (@ (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      "Creative Commons Attribution Share-Alike 4.0 International"))

(define %cc-by-sa-button
  '(a (@ (class "cc-button")
         (href "https://creativecommons.org/licenses/by-sa/4.0/"))
      (img (@ (src "https://licensebuttons.net/l/by-sa/4.0/80x15.png")))))

;;Creative Commons Attribution-NoDerivs 4.0 license (a.k.a. CC BY-ND) (#ccbynd)
(define %cc-by-nd-link
  '(a (@ (href "https://creativecommons.org/licenses/by-nd/4.0/"))
      "Creative Commons Attribution-NoDerivs 4.0"))

(define %cc-by-nd-button
  '(a (@ (class "cc-button")
         (href "https://creativecommons.org/licenses/by-nd/4.0/"))
      (img (@ (src "https://licensebuttons.net/l/by-nd/4.0/80x15.png")))))

(define %piwik-code
  '((script (@ (type "text/javascript") (src "/js/piwik.js")))
    (noscript
     (p (img (@ (src "//stats.dthompson.us/piwik.php?idsite=1")
                (style "border:0;")
                (alt "")))))))

(define (link name uri)
  `(a (@ (href ,uri)) ,name))

(define* (centered-image url #:optional alt)
  `(img (@ (class "centered-image")
           (src ,url)
           ,@(if alt
                 `((alt ,alt))
                 '()))))

(define (first-paragraph post)
  (let loop ((sxml (post-sxml post))
             (result '()))
    (match sxml
      (() (reverse result))
      ((or (('p ...) _ ...) (paragraph _ ...))
       (reverse (cons paragraph result)))
      ((head . tail)
       (loop tail (cons head result))))))

(define joyofsource.com-theme
  (theme #:name "joyofsource.com"
         #:layout
         (lambda (site title body)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))
              (title ,(string-append title " — " (site-title site)))
              ,(stylesheet "reset")
              ,(stylesheet "fonts")
              ,(stylesheet "joyofsource.com"))
             (body
              (div (@ (class "container"))
                   (div (@ (class "nav"))
                        (ul (li ,(link "janneke" "/"))
                            (li (@ (class "fade-text")) " ")
                            (li ,(link "contact" "/contact.html"))
                            (li ,(link "about" "/about.html"))
                            (li ,(link "blog" "/index.html"))
                            (li ,(link "projects" "/projects.html"))
                            (li ,(link "avatar" "/avatar.html"))))
                   ,@(if (not (equal? title "Recent Blog Posts")) '()
                       `((div (@ (class "right-box"))
                              ,(anchor (centered-image "images/feed.png" "atom")
                                       "/feed.xml"))))
                   ,body
                   (footer (@ (class "text-center"))
                           (p (@ (class "copyright"))
                              "© 2018,2019,2020,2021,2022,2023 <janneke@gnu.org>"
                              ,%cc-by-nd-button)
                           (p "The text and images on this site are
free culture works available under the " ,%cc-by-nd-link " license.")
                           (p "This website is built with "
                              (a (@ (href "http://haunt.dthompson.us"))
                                 "Haunt")
                              ", a static site generator written in "
                              (a (@ (href "https://gnu.org/software/guile"))
                                 "GNU Guile")
                              ".")))
              ,%piwik-code)))
         #:post-template
         (lambda (post)
           `((h1 (@ (class "title")),(post-ref post 'title))
             (div (@ (class "date"))
                  ,(date->string (post-date post)
                                 "~B ~d, ~Y"))
             (div (@ (class "post"))
                  ,(post-sxml post))))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
             (string-append "/" (or prefix "")
                            (site-post-slug site post) ".html"))

           `((h1 ,title)
             ,(map (lambda (post)
                     (let ((uri (string-append "/"
                                               (site-post-slug site post)
                                               ".html")))
                       `(div (@ (class "summary"))
                             (h2 (a (@ (href ,uri))
                                    ,(post-ref post 'title)))
                             (div (@ (class "date"))
                                  ,(date->string (post-date post)
                                                 "~B ~d, ~Y"))
                             (div (@ (class "post"))
                                  ,(first-paragraph post))
                             (a (@ (href ,uri)) "read more ➔"))))
                   posts)))))

(define* (collections #:key (file-name "index.html"))
  `(("Recent Blog Posts" ,file-name ,posts/reverse-chronological)))

(define parse-lang
  (let ((rx (make-regexp "-*-[ ]+([a-z]*)[ ]+-*-")))
    (lambda (port)
      (let ((line (read-line port)))
        (match:substring (regexp-exec rx line) 1)))))

(define (maybe-highlight-code lang source)
  (let ((lexer (match lang
                 ('scheme lex-scheme)
                 ('xml    lex-xml)
                 ('c      lex-c)
                 (_ #f))))
    (if lexer
        (highlights->sxml (highlight lexer source))
        source)))

(define (sxml-identity . args) args)

(define (highlight-code . tree)
  (sxml-match tree
    ((code (@ (class ,class) . ,attrs) ,source)
     (let ((lang (string->symbol
                  (string-drop class (string-length "language-")))))
       `(code (@ ,@attrs)
             ,(maybe-highlight-code lang source))))
    (,other other)))

(define (highlight-scheme code)
  `(pre (code ,(highlights->sxml (highlight lex-scheme code)))))

(define (raw-snippet code)
  `(pre (code ,(if (string? code) code (read-string code)))))

;; Markdown doesn't support video, so let's hack around that!  Find
;; <img> tags with a ".webm" source and substitute a <video> tag.
(define (media-hackery . tree)
  (sxml-match tree
    ((img (@ (src ,src) . ,attrs) . ,body)
     (if (string-suffix? ".webm" src)
         `(video (@ (src ,src) (controls "true"),@attrs) ,@body)
         tree))))

(define %commonmark-rules
  `((code . ,highlight-code)
    (img . ,media-hackery)
    (*text* . ,(lambda (tag str) str))
    (*default* . ,sxml-identity)))

(define (post-process-commonmark sxml)
  (pre-post-order sxml %commonmark-rules))

(define commonmark-reader*
  (make-reader (make-file-extension-matcher "md")
               (lambda (file)
                 (call-with-input-file file
                   (lambda (port)
                     (values (read-metadata-headers port)
                             (post-process-commonmark
                              (commonmark->sxml port))))))))

(define (static-page title file-name body)
  (lambda (site posts)
    (make-page file-name
               (with-layout joyofsource.com-theme site title body)
               sxml->html)))

(define* (project-page #:key name file-name description usage requirements
                       installation manual? license repo releases)
  (define body
    `((h1 ,name)
      ,description
      ,@(if usage
            `((h2 "Usage")
              ,usage)
            '())
      ,@(if manual?
         `((h2 "Documentation")
           (p ,(anchor "View the reference manual"
                       (string-append "/manuals/" repo "/index.html"))))
         '())
      (h2 "Releases")
      (ul ,(map (match-lambda
                  ((version date)
                   (let ((url (string-append repo "/-/archive/" version "/" name "-" version ".tar.gz")))
                     `(li ,(date->string date "~Y-~m-~d")
                          " — " ,version " — "
                          ,(anchor (string-append name "-" version ".tar.gz") url)))))
                releases))
      (h2 "Requirements")
      (ul ,(map (lambda (requirement)
                  `(li ,requirement))
                requirements))
      (h2 "License")
      (p ,license)
      (h2 "Source Code")
      ,@(let ((url repo))
          `((p ,name " is developed using the Git version control
system. The official repository is hosted at "
               ,(anchor url url))
            (h3 "Anonymous clone")
            (pre "git clone " ,url)))
      (h2 "Community")
      (p "Real-time discussion for " ,name " can be found on the "
         (code "#bootstrappable")
         " channel on the Freenode IRC network.")
      (h2 "Contributing")
      (p "Send patches and bug reports to "
         ,(anchor "janneke@gnu.org" "mailto:janneke@gnu.org")
         ".")))

  (static-page name (string-append "projects/" file-name) body))

(define* (contact-page #:key (company "Joy of Source") (name "janneke"))
  (static-page
   "contact"
   "contact.html"
   `((p ,company (br)
        "Helmond" (br)
        "Netherlands")
     (p ,(string-append name " -- +31 (0)6 16177916") (br)
        "kvk: 17282463" (br)
        ,(anchor "Algemene Voorwaarden" "images/joyofsource/algemene-voorwaarden.pdf") (br)
        "var: 2010-2018" (br))
     (p "If you're into social media, you can follow me on "
        ,(anchor "mastodon" "https://todon.nl/@janneke")
        ", or " ,(anchor "twitter" "https://twitter.com/janneke_gnu") "."))))

(define (about-page)
  (static-page
   "about"
   "about.html"
   `((h2 "Hi!")
     (p "janneke works as a freelance software engineer, mainly
for " ,(anchor "verum.com" "https://verum.com"))
     (p "janneke has a MS (ir) in Physics from " ,(anchor "Eindhoven
University of Technology (TU/e)" "https://tue.nl") ".")
     (p "If you're into social media, you can follow me on "
        ,(anchor "mastodon" "https://octodon.social/@janneke") "."
        ", or " ,(anchor "twitter" "https://twitter.com/janneke_gnu") "."))))

(define (projects-page)
  (static-page
   "projects"
   "projects.html"
   `((h1 "projects")
     (h2 "founder")
     (p ,(anchor "GNU Mes" "https://gnu.org/software/mes")
        " — Full Source Bootstrapping for GNU/Linux distributions.")
     (p ,(anchor "GNU LilyPond" "http://lilypond.org")
        " — Free Music Software for creating beautiful sheet music.")
     (p ,(anchor "the Mutopia Project" "http://www.mutopiaproject.org")
        " — Sheet music editions, free to download, modify, print,
copy, distribute, perform and record and editable in LilyPond
format.")
     (p ,(anchor "DOE040" "https://doe040.nl")
        " — A democratic school for people from ages 4-21.")
     (h2 "contributor")
     (p ,(anchor "GNU Guix" "https://gnu.org/software/guix")
        " — A distribution of the GNU operating system centered on the GNU Guix package manager.")
     (p ,(anchor "Gash" "https://savannah.nongnu.org/projects/gash")
        " — A POSIX compliant sh replacement in GNU Guile.")
     (p ,(anchor "Bootstrappable TinyCC" "projects/bootstrappable-tcc.html")
        " — A patch set for TCC to make it bootstrappable."))))

(define* (avatar-page #:key (file-name "avatar.html"))
  (static-page
   "avatar"
   file-name
   `((h1 "Avatar®")
     (p "The solutions to the difficulties that our planet is facing
lie in awareness.  We will find them by advancing our skills of
managing consciousness.")
     (p "If you are facing difficulties in your personal or
professional life, if you want to discover how your consciousness
operates, if you want to find out who you really are and what
awareness is, or if you are interested in contributing to help saving
our planet, then you may want to consider joining our Avatar
program.")
     (p ,(anchor "Avatar" "https://avatarepc.com")
        " is a nine-day self-empowerment training delivered by an
Avatar Master. The Avatar tools are a synergy of exercises, drills,
and procedures that when properly understood and used increase your
ability to live deliberately.  They are remarkably effective and
efficient tools for taking control of your life.")
     (h2 "The Mission of Avatar")
     (p "The ",(anchor "mission of Avatar" "https://avatarepc.com/what-is-avatar.html#mission")
        " in the world is to catalyze the
integration of belief systems.  When we perceive that the only
difference between us is our beliefs and that beliefs can be created
or discreated with ease, the right and wrong game will wind down, a
co-create game will unfold, and world peace will ensue."))))

(define (bootstrappable-tcc-page)
  (project-page
   #:name "Bootstrappable TinyCC"
   #:file-name "bootstrappable-tcc.html"
   #:description
   `((p ,(anchor "TCC" "https://tinycc.org") ", also referred to as
\"TinyCC\", is a small and fast C compiler written in C.  It supports
ANSI C with GNU and extensions and most of the C99 standard.")
     (p "Our vision of making TCC a key ingredient of the full source
bootstrap and making it easier to bootstrap by rewriting needlessly
complex or even \"exotic\" C99 constructs sadly and unexpectedly was "
        ,(anchor "rejected" "http://lists.gnu.org/archive/html/tinycc-devel/2017-09/msg00019.html")
        " by the TinyCC developers.")
     (p "Meanwhile, ",(anchor "bootstrappable TinyCC" "https://gitlab.com/janneke/tcc")
        " is a friendly fork to allow TCC to be used in a full source
bootstrap for a GNU/Linux distribution."))
   #:requirements '("GNU Mes >= 0.25"
                    "MesCC-Tools >= 1.5.0")
   #:license "GNU LGPLv2.1+"
   #:repo "https://gitlab.com/janneke/tinycc/"
   #:manual? #f
   #:releases `(("mes-0.25" ,(date 2023 11 14))
                ("mes-0.24" ,(date 2022 05 02))
                ("mes-0.23.1" ,(date 2021 12 21))
                ("mes-0.23" ,(date 2021 03 14))
                ("mes-0.22" ,(date 2020 01 23))
                ("mes-0.21" ,(date 2020 02 22))
                ("mes-0.20" ,(date 2019 09 09))
                ("mes-0.19" ,(date 2018 12 16))
                ("mes-0.18.1" ,(date 2018 10 11))
                ("mes-0.18" ,(date 2018 10 07))
                ("mes-0.17.1" ,(date 2018 08 14))
                ("mes-0.16" ,(date 2018 06 25))
                ("mes-0.15" ,(date 2018 06 11))
                ("mes-0.14" ,(date 2018 05 22))
                ("mes-0.13" ,(date 2018 05 03)))))

(site #:title "joyofsource.com"
      #:domain "joyofsource.com"
      #:default-metadata
      '((author . "janneke")
        (email  . "janneke@gnu.org"))
      #:readers (list commonmark-reader* texinfo-reader)
      #:builders (list (blog #:theme joyofsource.com-theme #:collections (collections))
                       (atom-feed)
                       (atom-feeds-by-tag)
                       (about-page)
                       (contact-page)
                       (projects-page)
                       (bootstrappable-tcc-page)
                       (avatar-page)
                       (static-directory "js")
                       (static-directory "css")
                       (static-directory "fonts")
                       (static-directory "images")
                       (static-directory "videos")
                       (static-directory "src")
                       (static-directory "manuals")))
