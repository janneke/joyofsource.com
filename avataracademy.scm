;;; Copyright © 2018 David Thompson <davet@gnu.org>
;;; Copyright © 2018 <janneke@gnu.org>
;;;
;;; This file is part of avataracademy.com.
;;;
;;; avataracademy.com is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; avataracademy.com is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with avataracademy.com.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; The text and images on this site are free culture works available
;;; under the Creative Commons Attribution-NoDerivs 4.0 license, see
;;; https://creativecommons.org/licenses/by-nd/4.0/

(include "haunt.scm")

(define (static-page title file-name body)
  (lambda (site posts)
    (make-page file-name
               (with-layout avataracademy.com-theme site title body)
               sxml->html)))

(define avataracademy.com-theme
  (theme #:name "avataracademy.com"
         #:layout
         (lambda (site title body)
           `((doctype "html")
             (head
              (meta (@ (charset "utf-8")))
              (title ,(string-append title " — " (site-title site)))
              ,(stylesheet "reset")
              ,(stylesheet "fonts")
              ,(stylesheet "avataracademy.com"))
             (body
              (div (@ (class "container"))
                   (div (@ (class "nav"))
                        (ul (li ,(link "avatar" "/"))
                            (li (@ (class "fade-text")) " ")
                            (li ,(link "contact" "/contact.html"))
                            (li ,(link "about" "/about.html"))
                            (li ,(link "blog" "/blog.html"))
                            (li ,(link "projects" "/projects.html"))))
                   ,@(if (not (equal? title "Recent Blog Posts")) '()
                       `((div (@ (class "right-box"))
                              ,(anchor (centered-image "images/feed.png" "atom")
                                       "/feed.xml"))))
                   ,body
                   (footer (@ (class "text-center"))
                           (p (@ (class "copyright"))
                              "© 2018 <jan@avatar.nl>"
                              ,%cc-by-nd-button)
                           (p "The text and images on this site are
free culture works available under the " ,%cc-by-nd-link " license.")
                           (p "This website is built with "
                              (a (@ (href "http://haunt.dthompson.us"))
                                 "Haunt")
                              ", a static site generator written in "
                              (a (@ (href "https://gnu.org/software/guile"))
                                 "GNU Guile")
                              ".")))
              ,%piwik-code)))
         #:post-template
         (lambda (post)
           `((h1 (@ (class "title")),(post-ref post 'title))
             (div (@ (class "date"))
                  ,(date->string (post-date post)
                                 "~B ~d, ~Y"))
             (div (@ (class "post"))
                  ,(post-sxml post))))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
             (string-append "/" (or prefix "")
                            (site-post-slug site post) ".html"))

           `((h1 ,title)
             ,(map (lambda (post)
                     (let ((uri (string-append "/"
                                               (site-post-slug site post)
                                               ".html")))
                       `(div (@ (class "summary"))
                             (h2 (a (@ (href ,uri))
                                    ,(post-ref post 'title)))
                             (div (@ (class "date"))
                                  ,(date->string (post-date post)
                                                 "~B ~d, ~Y"))
                             (div (@ (class "post"))
                                  ,(first-paragraph post))
                             (a (@ (href ,uri)) "read more ➔"))))
                   posts)))))

(site #:title "avataracademy.com"
      #:domain "avataracademy.com"
      #:default-metadata
      '((author . "Jan Nieuwenhuizen")
        (email  . "jan@avatar.nl"))
      #:readers (list commonmark-reader*)
      #:builders (list (avatar-page #:file-name "index.html")
                       (atom-feed)
                       (atom-feeds-by-tag)
                       (about-page)
                       (contact-page #:company "Avatar Academy" #:name "Jan Nieuwenhuizen")
                       (projects-page)
                       (bootstrappable-tcc-page)
                       (blog #:theme avataracademy.com-theme #:collections (collections #:file-name "blog.html"))
                       (static-directory "js")
                       (static-directory "css")
                       (static-directory "fonts")
                       (static-directory "images")
                       (static-directory "videos")
                       (static-directory "src")
                       (static-directory "manuals")))
